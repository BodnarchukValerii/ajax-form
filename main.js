$(document).ready(function() {

	let main = $(".container");

	function insertData (data) {
		let id = data["id"];
		$(".col-sm-name").append("<div>" + data["name"] + "</div>");
		$(".col-sm-author").append("<div>" + data["author"] + "</div>");
		$(".col-sm-genre").append("<div class='flex'><div>" + data["genre"] + "</div><button id='#remove' class='remove' data-id=" + id + ">x</button></div>");
	}

	// GET

	let getFilter = "id=5";
	let getPaginate = "_page=5&limit=20";
	let getSort = "_sort=id,genre,name&_order=desc";
	let getSlice = "_start=12&_end=30";
	let getOperators = "id_gte=2&id_lte=4";

	var getData = function(E) {
		$.get("http://localhost:3004/Books/?" + E,  function(result, textStatus, request){
			$.each(result, function(i, result) {
				insertData(result)
			});
		},'json');
	}
	getData();

	// getFullTextSearch

	$("#button-search").on('click', function(e){
		e.preventDefault();
		let inputSearch = $("input[name='search']").val();
		console.log(inputSearch);
		let getFullTextSearch = "q=" + inputSearch + "";
		$('.col-sm-name').html('');
		$('.col-sm-genre').html('');
		$('.col-sm-author').html('');
		getData(getFullTextSearch)
	});

	// DELETE

	main.delegate('.remove', 'click', function() {
		let flex = $(this).closest(".flex"),
			indexOfThis = $(".remove").index($(this));
		$.ajax({
			url: 'http://localhost:3004/Books/' + $(this).attr('data-id'),
	    	type: 'DELETE',
	    	success: function(result) {
				let deleteAuthor = $(".col-sm-author").find("div:eq(" + indexOfThis + ")").remove();
				let deleteName = $(".col-sm-name").find("div:eq(" + indexOfThis + ")").remove();
				flex.remove();
			}
		});
	});

	// Post

	$("#button1").on("click", function(e) {
		e.preventDefault();
		sendData();
	});

	function sendData() {
		let inputName = $("input[name='name']").val(),
			inputGenre = $("input[name='genre']").val(),
			inputAuthor = $("input[name='author']").val();
		$.ajax({
	        type: 'POST',
	        url: 'http://localhost:3004/Books',
	        data: {
	        	author: inputAuthor,
	        	genre: inputGenre,
	        	name: inputName
	        },
	        success: function(data) {
	        	insertData(data);
	        }
	    });
	};

	// PUT

	// main.delegate('.put-circle', 'click', function() {
	// 	$.ajax({
	//     	url: 'http://localhost:3004/Books/' + $(this).attr('data-id'),
	//     	type: 'PUT',
	//     	data: {
	//     		read: true
	//     	},
	//     	success: function(response) {
	//      		console.log("OYE");
	//     	}
	// 	});
	// });

});